import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import { AccordionDecorator } from 'components/AccordionDecorator';
import { TodoItemForm, TodoItemFormProps } from 'components/TodoItemForm';
import React from 'react';

const defaultProps: TodoItemFormProps = {
  onCancelClick: action('onCancelClick'),
  onDescriptionChange: action('onDescriptionChange'),
  onNameChange: action('onNameChange'),
  onSaveClick: action('onSaveClick')
};

storiesOf('components/TodoItemForm', module)
  .addDecorator(AccordionDecorator)
  .add('initial state', () => <TodoItemForm {...defaultProps} />)
  .add('empty, open', () => <TodoItemForm {...defaultProps} active={true} />)
  .add('with filled \'Name\' field', () => (
    <TodoItemForm {...defaultProps} name="New item name" />
  ))
  .add('with filled \'Description\' field', () => (
    <TodoItemForm
      {...defaultProps}
      active={true}
      description="New item description"
    />
  ))
  .add('with both fields filled', () => (
    <TodoItemForm
      {...defaultProps}
      active={true}
      description="New item description"
      name="New item name"
    />
  ));
