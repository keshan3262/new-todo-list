import 'components/TodoItemView.css';
import { noop } from 'lodash';
import React from 'react';
import {
  Accordion,
  AccordionTitleProps,
  Button,
  ButtonProps,
  Checkbox,
  CheckboxProps,
  Grid,
  Icon,
  Rating,
  RatingProps,
  Segment
} from 'semantic-ui-react';
import { TodoItem } from 'typings/TodoItem';

type TodoItemViewClickCallback = (props: TodoItemViewProps) => void;
type TodoItemViewCheckCallback = (
  value: CheckboxProps['checked'],
  props: TodoItemViewProps
) => void;

export interface TodoItemViewProps extends TodoItem {
  active?: AccordionTitleProps['active'];
  onClick?: TodoItemViewClickCallback;
  onDoneChange?: TodoItemViewCheckCallback;
  onPinnedChange?: TodoItemViewCheckCallback;
  onDeleteClick?: TodoItemViewClickCallback;
  pinned?: boolean;
}

const stopPropagationCallback = <Args extends Array<unknown>>(
  event: React.SyntheticEvent,
  callback: ((...args: Args) => void) | undefined,
  ...args: Args
) => {
  event.stopPropagation();

  if (callback) {
    callback(...args);
  }
};

export class TodoItemView extends React.PureComponent<TodoItemViewProps> {
  render (): React.ReactChild {
    const { active, description, done, pinned, name, uuid } = this.props;

    return (
      <React.Fragment>
        <Accordion.Title
          active={active}
          index={uuid}
          onClick={this.handleTitleClick}
        >
          <Grid>
            <Grid.Row verticalAlign="middle">
              <Grid.Column computer={12} mobile={11} tablet={12}>
                {description && <Icon name="dropdown" />}

                {name}
              </Grid.Column>

              <Grid.Column computer={4} mobile={5} tablet={4} textAlign="right">
                <Segment.Inline className="TodoItemView__inline">
                  <Rating
                    icon="star"
                    name="pinned"
                    onRate={this.handlePinnedChange}
                    rating={pinned ? 1 : 0}
                  />

                  <Checkbox
                    checked={done}
                    name="done"
                    onChange={this.handleDoneChange}
                  />

                  <Button
                    icon="delete"
                    name="delete"
                    negative={true}
                    onClick={this.handleDeleteClick}
                  />
                </Segment.Inline>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Accordion.Title>

        {description && (
          <Accordion.Content active={active}>{description}</Accordion.Content>
        )}
      </React.Fragment>
    );
  }

  private handleTitleClick = () => {
    const { onClick = noop } = this.props;
    onClick(this.props);
  }

  private handleDeleteClick: ButtonProps['onClick'] = event => {
    stopPropagationCallback(event, this.props.onDeleteClick, this.props);
  }

  private handleDoneChange: CheckboxProps['onChange'] = (
    event,
    { checked }
  ) => {
    stopPropagationCallback(
      event,
      this.props.onDoneChange,
      checked,
      this.props
    );
  }

  private handlePinnedChange: RatingProps['onRate'] = (event, { rating }) => {
    stopPropagationCallback(
      event,
      this.props.onPinnedChange,
      rating === 1,
      this.props
    );
  }
}
