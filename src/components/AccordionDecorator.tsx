import { StoryDecorator } from '@storybook/react';
import React from 'react';
import { Accordion } from 'semantic-ui-react';

export const AccordionDecorator: StoryDecorator = storyFn => (
  <Accordion fluid={true} styled={true}>
    {storyFn()}
  </Accordion>
);
