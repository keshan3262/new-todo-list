import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import { AccordionDecorator } from 'components/AccordionDecorator';
import { TodoItemView, TodoItemViewProps } from 'components/TodoItemView';
import React from 'react';

const defaultProps: TodoItemViewProps = {
  active: false,
  description: 'Item description',
  done: false,
  name: 'Todo item',
  onClick: action('onClick'),
  onDeleteClick: action('onDeleteClick'),
  onDoneChange: action('onDoneChange'),
  onPinnedChange: action('onPinnedChange'),
  pinned: false,
  uuid: '45745c60-7b1a-11e8-9c9c-2d42b21b1a3e'
};

storiesOf('components/TodoItemView', module)
  .addDecorator(AccordionDecorator)
  .add('without description', () => (
    <TodoItemView {...defaultProps} description={undefined} />
  ))
  .add('inactive', () => <TodoItemView {...defaultProps} />)
  .add('active', () => <TodoItemView {...defaultProps} active={true} />)
  .add('done', () => <TodoItemView {...defaultProps} done={true} />)
  .add('pinned', () => <TodoItemView {...defaultProps} pinned={true} />);
