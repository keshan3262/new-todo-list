import 'components/TodoItemForm.css';
import React from 'react';
import {
  Accordion,
  ButtonProps,
  Form,
  InputProps,
  Segment,
  TextArea
} from 'semantic-ui-react';
import { TodoItem } from 'typings/TodoItem';

export interface TodoItemFormProps {
  active?: boolean;
  description?: TodoItem['description'];
  name?: TodoItem['name'];
  onCancelClick?: ButtonProps['onClick'];
  onDescriptionChange?: InputProps['onChange'];
  onNameChange?: InputProps['onChange'];
  onSaveClick?: ButtonProps['onClick'];
}

export class TodoItemForm extends React.PureComponent<TodoItemFormProps> {
  render (): React.ReactChild {
    const {
      active,
      description = '',
      name = '',
      onCancelClick,
      onDescriptionChange,
      onNameChange,
      onSaveClick
    } = this.props;

    const activeDependentPanelProps = active === undefined ? {} : { active };

    const panels = [
      {
        ...activeDependentPanelProps,
        content: {
          as: TextArea,
          name: 'description',
          onChange: onDescriptionChange,
          placeholder: 'Enter todo item description here',
          value: description
        },
        key: 'description',
        title: 'Description (optional)'
      }
    ];

    return (
      <Form as={Segment}>
        <Form.Group>
          <Form.Input
            label="Name"
            name="name"
            onChange={onNameChange}
            placeholder="Name"
            value={name}
            width={16}
          />
        </Form.Group>

        <Accordion as={Form.Field} panels={panels} styled={true} />

        <Form.Group className="TodoItemForm__ButtonsGroup">
          <Form.Button
            icon="save"
            name="save"
            positive={true}
            onClick={onSaveClick}
            disabled={name.length === 0}
          />

          <Form.Button
            icon="cancel"
            name="cancel"
            negative={true}
            onClick={onCancelClick}
          />
        </Form.Group>
      </Form>
    );
  }
}
