import { TodoItemView } from 'components/TodoItemView';
import Enzyme from 'enzyme';
import React from 'react';
import {
  Accordion,
  Button,
  Checkbox,
  Rating,
  RatingIcon
} from 'semantic-ui-react';

describe('components/TodoItemView', () => {
  const onClick = jest.fn();
  const onDoneChange = jest.fn();
  const onPinnedChange = jest.fn();
  const onDeleteClick = jest.fn();

  const itemProps = {
    active: false,
    description: 'Item description',
    done: false,
    index: 0,
    name: 'Todo item',
    onClick,
    onDeleteClick,
    onDoneChange,
    onPinnedChange,
    pinned: false,
    uuid: '45745c60-7b1a-11e8-9c9c-2d42b21b1a3e'
  };

  const item = Enzyme.mount(<TodoItemView {...itemProps} />);

  beforeEach(() => {
    onClick.mockClear();
    onDoneChange.mockClear();
    onPinnedChange.mockClear();
    onDeleteClick.mockClear();
  });

  it('should call onClick on title click', () => {
    const itemTitle = item.find(Accordion.Title);
    itemTitle.simulate('click');

    expect(onClick).toHaveBeenCalled();
  });

  it('should call onDeleteClick on \'Delete\' button click but not onClick', () => {
    const deleteButton = item.find(Button).filter({ name: 'delete' });
    deleteButton.simulate('click');

    expect(onDeleteClick).toHaveBeenCalledWith(itemProps);
    expect(onClick).not.toHaveBeenCalled();
  });

  it('should call onDoneChange on checkbox check but not onClick', () => {
    const doneCheckbox = item.find(Checkbox).filter({ name: 'done' });
    doneCheckbox.simulate('change');

    expect(onDoneChange).toHaveBeenCalledWith(true, itemProps);
    expect(onClick).not.toHaveBeenCalled();
  });

  it('should call onPinnedChange on checkbox check but on onClick', () => {
    const pinnedStar = item
      .find(Rating)
      .filter({ name: 'pinned' })
      .find(RatingIcon);
    pinnedStar.simulate('click');

    expect(onPinnedChange).toHaveBeenCalledWith(true, itemProps);
    expect(onClick).not.toHaveBeenCalled();
  });
});
