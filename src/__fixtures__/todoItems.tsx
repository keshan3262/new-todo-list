import { TodoListProps } from 'layouts/TodoList';
import { times } from 'lodash';

const todoItems: TodoListProps['items'] = times(4, index => ({
  description: index !== 0 ? `Todo item ${index + 1} description` : '',
  done: index === 1,
  name: `Todo item ${index + 1}`,
  pinned: index === 2,
  uuid: `45745c60-7b1a-11e8-9c9c-2d42b21b1a3${index}`
}));

export default todoItems;
