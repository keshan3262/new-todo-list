import React from 'react';
import { TodoListScreen } from 'screens/TodoList';

class App extends React.Component {
  render (): React.ReactChild {
    return (
      <div className="App">
        <TodoListScreen />
      </div>
    );
  }
}

export default App;
