import Enzyme from 'enzyme';
import ReactSixteenAdapter from 'enzyme-adapter-react-16';
// tslint:disable-next-line no-import-side-effect
import 'jest-localstorage-mock';

Enzyme.configure({ adapter: new ReactSixteenAdapter() });

const TEST_TIMEOUT = 60 * 1e3;
// tslint:disable-next-line no-object-mutation
jasmine.DEFAULT_TIMEOUT_INTERVAL = TEST_TIMEOUT;
jest.setTimeout(TEST_TIMEOUT);

jest.useFakeTimers();
