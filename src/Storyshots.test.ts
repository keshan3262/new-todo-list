// There is OK to use any
// tslint:disable:no-any no-commented-out-code

import addonStoryshots, {
  multiSnapshotWithOptions
  // @ts-ignore
} from '@storybook/addon-storyshots';

addonStoryshots({
  integrityOptions: { cwd: __dirname }, // it will start searching from the current directory
  test: multiSnapshotWithOptions({})
});
