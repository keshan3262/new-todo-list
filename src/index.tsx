// tslint:enable
import React from 'react';
import ReactDOM from 'react-dom';

/** SIDE EFFECTS */
// tslint:disable-next-line: no-import-side-effect
import 'prelude';

import App from 'App';

const app = (
  <React.Fragment>
    <App />
  </React.Fragment>
);

ReactDOM.render(app, document.getElementById('root') as HTMLElement);
