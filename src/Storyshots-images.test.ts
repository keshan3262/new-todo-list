// There is OK to use any
// tslint:disable:no-any no-commented-out-code
// @ts-ignore
import addonStoryshots from '@storybook/addon-storyshots';
// @ts-ignore
import { imageSnapshot } from '@storybook/addon-storyshots-puppeteer';
import * as process from 'process';
import * as puppeteer from 'puppeteer';

const getMatchOptions = () => ({
  customDiffConfig: {
    threshold: 0.001
  },
  failureThreshold: 0.001,
  failureThresholdType: 'percent'
});

const storybookUrl = process.env.STORYBOOK_URL || 'http://localhost:6006/';

const getCustomBrowser = async (): Promise<puppeteer.Browser> => {
  try {
    const browser = await ((puppeteer as any)
      .default as typeof puppeteer).launch({
      args: [
        '--no-sandbox ',
        '--disable-setuid-sandbox',
        '--enable-font-antialiasing',
        '--font-render-hinting=none',
        '--headless',
        '--disable-gpu',
        '--disable-dev-shm-usage',
        '--single-process'
      ],
      defaultViewport: {
        // 1440x900
        height: 900,
        width: 1440
      },
      dumpio: true
    });
    console.log('SUCCESSFUL started puppeteer');
    browser.on('targetdestroyed', () => {
      console.log('Page closed. Closing browser');
      browser.close();
    });
    return browser;
  } catch (e) {
    console.error('FAILED TO START puppeteer');
    console.error(e.name, e.message);
    console.error(e.stack);
    throw e;
  }
};

const getGotoOptions = () => ({
  // make sure that built storybook is used!
  // because dev version uses web socket
  // and networkidle0 check would fail because of it
  waitUntil: 'networkidle0'
});

addonStoryshots({
  suite: 'Image storyshots',
  test: imageSnapshot({
    getCustomBrowser,
    getGotoOptions,
    getMatchOptions,
    storyKindRegex: /^((?!.*?\[NO-IMAGE\]).)*$/,
    storyNameRegex: /^((?!.*?\[NO-IMAGE\]).)*$/,
    storybookUrl
  })
});
