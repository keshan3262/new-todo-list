import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import todoItems from '__fixtures__/todoItems';
import { TodoList } from 'layouts/TodoList';
import React from 'react';

const defaultProps = {
  items: todoItems,
  onChange: action('onChange')
};

storiesOf('layouts/TodoList', module)
  .add('empty', () => <TodoList {...defaultProps} items={[]} />)
  .add('all items aren\'t active', () => (
    <TodoList {...defaultProps} activeItemsIds={[]} />
  ))
  .add('all items are active', () => (
    <TodoList
      {...defaultProps}
      activeItemsIds={todoItems.map(({ uuid }) => uuid)}
    />
  ))
  .add('items\' activation isn\'t controlled by properties', () => (
    <TodoList {...defaultProps} />
  ))
  .add('item is being created', () => (
    <TodoList {...defaultProps} itemBeingCreated={true} />
  ));
