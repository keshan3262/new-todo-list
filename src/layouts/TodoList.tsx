import { TodoItemForm, TodoItemFormProps } from 'components/TodoItemForm';
import { TodoItemView, TodoItemViewProps } from 'components/TodoItemView';
import 'layouts/TodoList.css';
import { noop } from 'lodash';
import memoizeOne from 'memoize-one';
import React from 'react';
import { Accordion, Button, Grid, Header, Segment } from 'semantic-ui-react';
import { TodoItem } from 'typings/TodoItem';
import { v4 as generateUuid } from 'uuid';

export interface TodoListProps {
  /** WARNING: it should be used for snapshot tests only! */
  activeItemsIds?: Array<TodoItemViewProps['uuid']>;
  /** WARNING: it should be used for snapshot tests only! */
  itemBeingCreated?: boolean;
  items: TodoItem[];
  onChange?: (newItems: TodoItem[]) => void;
}

export interface TodoListState {
  activeItemsIds: Array<TodoItemViewProps['uuid']>;
  itemBeingCreated: boolean;
  newItemDescription: string;
  newItemName: string;
}

export class TodoList extends React.Component<TodoListProps, TodoListState> {
  state: TodoListState = {
    activeItemsIds: [],
    itemBeingCreated: false,
    newItemDescription: '',
    newItemName: ''
  };

  private renderItemsListContent = memoizeOne(
    (
      activeItemsIds: TodoListState['activeItemsIds'],
      items: TodoListProps['items']
    ) => {
      if (items.length === 0) {
        return <Accordion.Title>No items are here yet</Accordion.Title>;
      }

      return [...items]
        .sort(this.compareByPinned)
        .map(item => (
          <TodoItemView
            {...item}
            active={activeItemsIds.includes(item.uuid)}
            key={item.uuid}
            onClick={this.handleItemClick}
            onDeleteClick={this.handleDeleteClick}
            onDoneChange={this.handleDoneChange}
            onPinnedChange={this.handlePinnedChange}
          />
        ));
    }
  );

  render (): React.ReactChild {
    const {
      activeItemsIds = this.state.activeItemsIds,
      itemBeingCreated = this.state.itemBeingCreated,
      items
    } = this.props;

    return (
      <Grid className="TodoList">
        <Grid.Row centered={true}>
          <Grid.Column computer={12} tablet={14} mobile={16}>
            <Segment basic={true} attached="top" className="TodoList__Title">
              <Grid>
                <Grid.Row verticalAlign="middle">
                  <Grid.Column width={12}>
                    <Header size="large">TODO list</Header>
                  </Grid.Column>

                  <Grid.Column width={4} textAlign="right">
                    <Button
                      className="TodoList__Title__Button"
                      positive={true}
                      icon="add"
                      name="add"
                      onClick={this.handleAddButtonClick}
                    />
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Segment>

            <Accordion fluid={true} styled={true}>
              {this.renderItemsListContent(activeItemsIds, items)}

              {itemBeingCreated && this.renderTodoItemForm()}
            </Accordion>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }

  private renderTodoItemForm = () => {
    const { newItemDescription, newItemName } = this.state;

    return (
      <TodoItemForm
        description={newItemDescription}
        name={newItemName}
        onCancelClick={this.handleCancelClick}
        onDescriptionChange={this.handleDescriptionChange}
        onNameChange={this.handleNameChange}
        onSaveClick={this.handleSaveClick}
      />
    );
  }

  private compareByPinned = (
    { pinned: item1Pinned = false }: TodoItem,
    { pinned: item2Pinned = false }: TodoItem
  ) => {
    return Number(item2Pinned) - Number(item1Pinned);
  }

  private handleDeleteClick: TodoItemViewProps['onDeleteClick'] = ({
    uuid: itemToDeleteId
  }) => {
    const { items, onChange } = this.props;

    const newItems = items.filter(({ uuid }) => uuid !== itemToDeleteId);

    if (!onChange) {
      return;
    }

    onChange(newItems);
    this.setState({
      activeItemsIds: this.state.activeItemsIds.filter(
        activeItemId => activeItemId !== itemToDeleteId
      )
    });
  }

  private handleTodoItemFlagChange = (
    uuid: TodoItem['uuid'],
    flag: 'done' | 'pinned',
    value?: boolean
  ) => {
    const { items, onChange = noop } = this.props;

    const newItems = items.map(item =>
      uuid === item.uuid
        ? {
            ...item,
            [flag]: value
          }
        : item
    );

    onChange(newItems);
  }

  private handleDoneChange: TodoItemViewProps['onDoneChange'] = (
    value,
    { uuid }
  ) => {
    this.handleTodoItemFlagChange(uuid, 'done', value);
  }

  private handlePinnedChange: TodoItemViewProps['onPinnedChange'] = (
    value,
    { uuid }
  ) => {
    this.handleTodoItemFlagChange(uuid, 'pinned', value);
  }

  private handleAddButtonClick = () => {
    this.setState({ itemBeingCreated: true });
  }

  private closeNewItemForm = (callback?: () => void) =>
    this.setState(
      {
        itemBeingCreated: false,
        newItemDescription: '',
        newItemName: ''
      },
      callback
    )

  private handleCancelClick = () => {
    this.closeNewItemForm();
  }

  private handleDescriptionChange: TodoItemFormProps['onDescriptionChange'] = (
    event,
    { value }
  ) => {
    this.setState({ newItemDescription: value });
  }

  private handleNameChange: TodoItemFormProps['onNameChange'] = (
    event,
    { value }
  ) => {
    this.setState({ newItemName: value });
  }

  private handleSaveClick: TodoItemFormProps['onSaveClick'] = () => {
    const { items, onChange = noop } = this.props;
    const { newItemDescription, newItemName } = this.state;

    this.closeNewItemForm(() =>
      onChange([
        ...items,
        {
          description: newItemDescription,
          done: false,
          name: newItemName,
          pinned: false,
          uuid: generateUuid()
        }
      ])
    );
  }

  private handleItemClick: TodoItemViewProps['onClick'] = ({ uuid }) => {
    const { activeItemsIds } = this.state;

    if (activeItemsIds.includes(uuid)) {
      this.setState({
        activeItemsIds: activeItemsIds.filter(itemId => itemId !== uuid)
      });
    } else {
      this.setState({
        activeItemsIds: [...activeItemsIds, uuid]
      });
    }
  }
}
