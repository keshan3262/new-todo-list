import todoItems from '__fixtures__/todoItems';
import { TodoItemForm } from 'components/TodoItemForm';
import { TodoItemView } from 'components/TodoItemView';
import Enzyme from 'enzyme';
import { TodoList } from 'layouts/TodoList';
import { omit } from 'lodash';
import React from 'react';
import { Button, Checkbox, RatingIcon } from 'semantic-ui-react';

describe('layouts/TodoList', () => {
  const onChange = jest.fn();
  let todoList = Enzyme.mount(<div />);

  const findTodoItemViewByUuid = (uuid: string) =>
    todoList.find(TodoItemView).filter({ uuid });

  const findElementByName = <P extends {}>(
    wrapper: Enzyme.ReactWrapper,
    component: React.ComponentType<P> | string,
    name: string
  ) => {
    // type error appears without this 'if' clause
    if (typeof component === 'string') {
      return wrapper.find(component).filter({ name });
    }

    return wrapper.find(component).filter({ name });
  };

  beforeEach(() => {
    onChange.mockClear();
    todoList = Enzyme.mount(<TodoList items={todoItems} onChange={onChange} />);
  });

  it('should activate multiple items after clicking on them', () => {
    const itemsIndicesToActivate = [1, 2];
    let todoListItems = todoList.find(TodoItemView);
    for (const itemIndex of itemsIndicesToActivate) {
      todoListItems.at(itemIndex).simulate('click');
    }

    todoListItems = todoList.find(TodoItemView);
    for (const itemIndex of itemsIndicesToActivate) {
      expect(todoListItems.at(itemIndex).props().active).toEqual(true);
    }
  });

  it('should deactivate an active item after clicking on it', () => {
    let todoListItem = todoList.find(TodoItemView).first();
    todoListItem.simulate('click');

    todoListItem = todoList.find(TodoItemView).first();
    if (todoListItem.props().active) {
      todoListItem.simulate('click');
      todoListItem = todoList.find(TodoItemView).first();

      expect(todoListItem.props().active).toEqual(false);
      return;
    }

    throw new Error('Item was not activated, cannot execute this test');
  });

  it('should call onChange callback to toggle item\'s \'Pinned\' flag on star click', () => {
    const todoListItemUuid = todoItems[0].uuid;
    let todoListItem = findTodoItemViewByUuid(todoListItemUuid);
    const initialPinned = todoListItem.props().pinned;

    let pinnedStar = todoListItem.find(RatingIcon);
    pinnedStar.simulate('click');

    const expectedNewItems = [
      { ...todoItems[0], pinned: !initialPinned },
      ...todoItems.slice(1)
    ];
    expect(onChange).toHaveBeenCalledWith(expectedNewItems);

    todoList.setProps({ items: expectedNewItems });
    todoListItem = findTodoItemViewByUuid(todoListItemUuid);
    pinnedStar = todoListItem.find(RatingIcon);
    pinnedStar.simulate('click');

    expect(onChange).toHaveBeenCalledWith(todoItems);
  });

  it('should call onChange callback to toggle item\'s \'Done\' flag on star click', () => {
    const todoListItemUuid = todoItems[0].uuid;
    let todoListItem = findTodoItemViewByUuid(todoListItemUuid);
    const initialDone = todoListItem.props().done;

    let doneCheckbox = todoListItem.find(Checkbox);
    doneCheckbox.simulate('change');

    const expectedNewItems = [
      { ...todoItems[0], done: !initialDone },
      ...todoItems.slice(1)
    ];
    expect(onChange).toHaveBeenCalledWith(expectedNewItems);

    todoList.setProps({ items: expectedNewItems });
    todoListItem = findTodoItemViewByUuid(todoListItemUuid);
    doneCheckbox = todoListItem.find(Checkbox);
    doneCheckbox.simulate('change');

    expect(onChange).toHaveBeenCalledWith(todoItems);
  });

  it('should call onChange callback to delete an item on \'Delete\' button click', () => {
    const todoListItemUuid = todoItems[0].uuid;
    const todoListItem = findTodoItemViewByUuid(todoListItemUuid);
    const deleteButton = findElementByName(todoListItem, Button, 'delete');
    deleteButton.simulate('click');

    expect(onChange).toHaveBeenCalledWith(todoItems.slice(1));
  });

  it('should show a form for todo item creation on \'Add\' button click', () => {
    const addButton = findElementByName(
      todoList.find('.TodoList__Title'),
      Button,
      'add'
    );
    addButton.simulate('click');

    const addItemForm = todoList.find(TodoItemForm);
    expect(addItemForm).toHaveLength(1);
  });

  it('should hide the form on \'Cancel\' button click', () => {
    const addButton = findElementByName(
      todoList.find('.TodoList__Title'),
      Button,
      'add'
    );
    addButton.simulate('click');

    let addItemForm = todoList.find(TodoItemForm);

    if (addItemForm.length === 1) {
      const cancelButton = findElementByName(addItemForm, Button, 'cancel');
      cancelButton.simulate('click');

      addItemForm = todoList.find(TodoItemForm);
      expect(addItemForm).toHaveLength(0);

      return;
    }

    throw new Error('Form did not appear, cannot execute this test');
  });

  it('should call onChange callback to create an item on \'Save\' button click', () => {
    const addButton = findElementByName(
      todoList.find('.TodoList__Title'),
      Button,
      'add'
    );
    addButton.simulate('click');

    let addItemForm = todoList.find(TodoItemForm);

    if (addItemForm.length === 1) {
      const newItemName = 'New todo item';
      const newItemDescription = 'New todo item description';
      const itemNameInput = findElementByName(addItemForm, 'input', 'name');
      const itemDescriptionInput = findElementByName(
        addItemForm,
        'textarea',
        'description'
      );
      const saveButton = findElementByName(addItemForm, Button, 'save');

      itemNameInput.simulate('change', { target: { value: newItemName } });
      itemDescriptionInput.simulate('change', {
        target: { value: newItemDescription }
      });
      saveButton.simulate('click');

      expect(onChange).toHaveBeenCalled();
      const newItems = onChange.mock.calls[0][0];
      expect(newItems.slice(0, newItems.length - 1)).toEqual(todoItems);
      const newItem = newItems[newItems.length - 1];
      expect(omit(newItem, 'uuid')).toEqual({
        description: newItemDescription,
        done: false,
        name: newItemName,
        pinned: false
      });
      expect(typeof newItem.uuid).toEqual('string');

      addItemForm = todoList.find(TodoItemForm);
      expect(addItemForm).toHaveLength(0);

      return;
    }

    throw new Error('Form did not appear, cannot execute this test');
  });
});
