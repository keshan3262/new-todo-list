import { TodoList, TodoListProps } from 'layouts/TodoList';
import React from 'react';

interface TodoListScreenState {
  items: TodoListProps['items'];
}

export class TodoListScreen extends React.Component<{}, TodoListScreenState> {
  static itemsLocalStorageKey = 'todoItems';

  constructor (props: {}) {
    super(props);

    const jsonEncodedItems = localStorage.getItem(
      TodoListScreen.itemsLocalStorageKey
    );
    this.state = {
      items: jsonEncodedItems ? JSON.parse(jsonEncodedItems) : []
    };
  }

  render (): React.ReactChild {
    return (
      <TodoList items={this.state.items} onChange={this.handleItemsChange} />
    );
  }

  private handleItemsChange: TodoListProps['onChange'] = newItems => {
    this.setState(
      {
        items: newItems
      },
      () =>
        localStorage.setItem(
          TodoListScreen.itemsLocalStorageKey,
          JSON.stringify(newItems)
        )
    );
  }
}
