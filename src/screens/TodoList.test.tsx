import todoItems from '__fixtures__/todoItems';
import Enzyme from 'enzyme';
import { TodoList } from 'layouts/TodoList';
import React from 'react';
import { TodoListScreen } from 'screens/TodoList';

describe('screens/TodoList', () => {
  it('initially passes empty array of todo items to layout if local storage is empty', () => {
    localStorage.removeItem(TodoListScreen.itemsLocalStorageKey);
    const screen = Enzyme.mount(<TodoListScreen />);

    const layout = screen.find(TodoList);
    expect(layout.props().items).toEqual([]);
  });

  beforeEach(() => {
    // @ts-ignore
    localStorage.setItem.mockClear();
  });

  it('initially passes array of todo items encoded in local storage if it\'s present', () => {
    localStorage.setItem(
      TodoListScreen.itemsLocalStorageKey,
      JSON.stringify(todoItems)
    );
    const screen = Enzyme.mount(<TodoListScreen />);

    const layout = screen.find(TodoList);
    expect(layout.props().items).toEqual(todoItems);
  });

  it('passes new list of items to the layout and saves it into local storage on callback call', () => {
    localStorage.removeItem(TodoListScreen.itemsLocalStorageKey);
    const screen = Enzyme.mount(<TodoListScreen />);
    let layout = screen.find(TodoList);
    layout.props().onChange!(todoItems);

    screen.update();
    layout = screen.find(TodoList);
    expect(layout.props().items).toEqual(todoItems);
    expect(localStorage.setItem).toHaveBeenCalledWith(
      TodoListScreen.itemsLocalStorageKey,
      JSON.stringify(todoItems)
    );
  });
});
