export interface TodoItem {
  description?: string;
  done?: boolean;
  name: string;
  pinned?: boolean;
  uuid: string;
}
