import { configure, addDecorator } from '@storybook/react';
import StoryRouter from 'storybook-react-router';
import requireContext from 'require-context.macro';
import { addParameters } from '@storybook/react';

addDecorator(StoryRouter());

addParameters({
  backgrounds: [
    {
      name: 'white',
      value: '#ffffff'
    },
    {
      name: 'grey',
      value: '#f0f2f7'
    },
    {
      name: 'black',
      value: '#000000'
    }
  ]
});

function loadStories() {
  /** SIDE EFFECTS */
  require('../src/prelude');
  const req = requireContext('../src', true, /.stories.tsx$/);
  req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);
