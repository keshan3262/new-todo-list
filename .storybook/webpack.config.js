const path = require('path');
const webpack = require('webpack');
const process = require('process');

function applyStorybookImageMocks(config) {
  if (!config.resolve.alias) {
    config.resolve.alias = {};
  }

  config.resolve.alias['components/RouteMapWidget'] = path.join(
    __dirname,
    '../src/components/__storyshot__mocks__/RouteMapWidget'
  );
}

module.exports = function({ config, mode }) {
  config.plugins.push(
    new webpack.IgnorePlugin(
      /\/(?:__image_snapshots__)|(?:__snapshots__)|(?:__diff_output__)\//
    ),
    new webpack.WatchIgnorePlugin([
      /__image_snapshots__/,
      /__snapshots__/,
      /__diff_output__/
    ])
  );
  config.plugins.push(new webpack.NamedModulesPlugin());

  if (!config.watchOptions) config.watchOptions = {};
  config.watchOptions.ignored = /\/(?:__image_snapshots__)|(?:__snapshots__)|(?:__diff_output__)\//;

  if (!config.resolve.plugins) config.resolve.plugins = [];

  if (process.env.STORYSHOT_IMAGE) {
    console.log('APPLY STORYSHOT_IMAGE MODE');
    applyStorybookImageMocks(config);
  }

  config.resolve.modules = [path.join(__dirname, '../src/'), 'node_modules'];

  if (typeof config.node !== 'object') {
    config.node = {};
  }
  Object.assign(config.node, {
    net: 'mock',
    tls: 'mock'
  });

  return config;
};
